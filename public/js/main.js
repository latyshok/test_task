'use strict';

const ajax = {
    get: function (path) {
        let xhr = new XMLHttpRequest();

        xhr.open('GET', path, false);
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        xhr.send();

        this.status = xhr.status === 200;
        this.response = xhr.responseText;

        return this;
    },
    post: function (path, form) {
        let formData = new FormData(form);

        // отослать
        let xhr = new XMLHttpRequest();
        xhr.open("POST", path);
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");

        xhr.send(formData);

        xhr.onreadystatechange = function() {
            if (this.readyState === 4) {
                ajax.status = this.status === 200;
                ajax.response = this.responseText;
            }
        };

        return this;
    },
    then: function (callback) {
        if (ajax.status)
            callback(ajax.response);
        return this;
    },
    catch: function (callback) {
        if (!ajax.status)
            callback(ajax.response);
        return this;
    },
    other: function (callback) {
        callback();
    }
};

function post(path, form)
{
    return new Promise(function(resolve, reject)
    {
        let formData = new FormData(form);

        // отослать
        let xhr = new XMLHttpRequest();
        xhr.open("POST", path);
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");

        xhr.send(formData);

        xhr.onreadystatechange = function() {
            if (this.readyState === 4) {
                if (this.status >= 200 && this.status < 400) {
                    resolve(this.response);
                } else {
                    var error = new Error(this.statusText);
                    error.code = this.status;
                    reject(error);
                }
            };
        };
    });
}


function reg(form){
    let email = new FormData(form).get('email'),
        button = form.querySelector('button'),
        error = form.querySelector('.error'),
        errors = '';
    button.innerText = '...';
    post('/user/reg', form)
        .then(
            response => {
                let data = JSON.parse(response);
                if(!data.status)
                    data.error.forEach(function(item, i, arr) {
                        errors += ('<p>' + item + '<p>');
                    });
                else {
                    form.reset();
                    form.innerHTML =
                        '<div class="success">' +
                            '<p>Учетная запись ' + email + ' успешно создана</p>' +
                            '<p><a href="/">Перейти на страницу авторизации</a></p>' +
                        '</div>';
                }

                error.innerHTML = errors;
                button.innerText = 'РЕГИСТРАЦИЯ';
            },
            error => {
                console.log(error);
            }
        );
}

function auth(form){
    let button = form.querySelector('button'),
        error = form.querySelector('.error'),
        success = form.querySelector('.success');

    button.innerText = '...';
    post('/user/login', form)
        .then(
            response => {
                console.log(response);
                let data = JSON.parse(response);
                if(!data.status)
                    error.innerHTML = '<p>Логин/Пароль введены неверно</p>';
                else
                   location.replace("/");
                button.innerText = 'Войти';
            },
            error => {
                console.log(error);
            }
        );
}

function create(form){
    let button = form.querySelector('button'),
        error = form.querySelector('.error'),
        success = form.querySelector('.success'),
        errors = '';
    button.innerText = '...';
    post('/profile/create', form)
        .then(
            response => {
                console.log(response);
                let data = JSON.parse(response);
                if(!data.status)
                    data.error.forEach(function(item, i, arr) {
                        errors += ('<p>' + item + '<p>');
                    });
                else {
                    location.replace("/profile");
                }

                error.innerHTML = errors;
                button.innerText = 'Создать анкету';
            },
            error => {
                console.log(error);
            }
        );
}


function edit(form){
    let button = form.querySelector('button'),
        error = form.querySelector('.error'),
        success = form.querySelector('.success'),
        errors = '';
    button.innerText = '...';
    post('/profile/edit', form)
        .then(
            response => {
                console.log(response);
                let data = JSON.parse(response);
                if(!data.status)
                    data.error.forEach(function(item, i, arr) {
                        errors += ('<p>' + item + '<p>');
                    });
                else {
                    location.replace("/profile");
                }

                error.innerHTML = errors;
                button.innerText = 'Редактировать анкету';
            },
            error => {
                console.log(error);
            }
        );
}

function del(id){
    var xhr = new XMLHttpRequest();

    var body = 'id=' + id;

    xhr.open("POST", '/profile/del', true);
    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");

    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if (this.readyState === 4) {
            if (this.status >= 200 && this.status < 400) {
                let data = JSON.parse(this.response);
                if (data.status)
                    location.replace("/profile");
            } else {
                var error = new Error(this.statusText);
                error.code = this.status;
                alert(error.code);
            }
        };
    };

    xhr.send(body);
}