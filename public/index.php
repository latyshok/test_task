<?php

use vendor\core\Router;

setlocale(LC_ALL, "ru_RU.UTF-8");
ini_set('display_errors', 1);

$query = trim($_SERVER['REQUEST_URI'], '/');

define('ROOT', dirname(__DIR__));
define('APP', dirname(__DIR__ ). '/app');

require_once '../vendor/libs/functions.php';

spl_autoload_register('my_autoloader');


session_start();

// Router::add('^page/(?P<action>[a-z-]+)/?(?P<alias>[a-z-]+)?$', ['controller' => 'Page']);
// Router::add('^page/(?P<alias>[a-z-]+)?$', ['controller' => 'Page', 'action' => 'view']);

Router::add('^$', ['controller' => 'Main', 'action' => 'index']);
// Router::add('/posts/add', ['controller' => 'Posts', 'action' => 'add']);﻿
Router::add('^(?P<controller>[a-z-]+)/?(?P<action>[a-z-]+)?$');


Router::dispatch( $query );
