<?php

function debug($arr){
    echo '<pre>' . print_r($arr, true) . '</pre>';
}

function my_autoloader($class)
{
    $file = ROOT . str_replace('\\', '/', "/$class") . '.php';
    if( is_file( $file ) ) {
        require_once $file;
    }
}