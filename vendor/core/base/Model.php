<?php
/**
 * Created by PhpStorm.
 * User: black
 * Date: 02.03.2019
 * Time: 14:50
 */

namespace vendor\core\base;


abstract  class Model
{
    protected $pdo;
    protected $table;
    protected $pk = 'id';

    public function __construct()
    {
        $this->pdo = Db::getInstance();
    }

    public function query($sql, $params = []){
        return $this->pdo->execute($sql, $params);
    }

    public function findAll()
    {
        $sql = "SELECT * FROM {$this->table}";
        return $this->pdo->query($sql);
    }

    public function findOne($id, $field = '')
    {
        $field = $field ?: $this->pk;
        $sql = "SELECT * FROM {$this->table} WHERE $field = ? LIMIT 1";
        return $this->pdo->query($sql, [$id]);
    }

    public function findBySql($sql, $params = [])
    {
        return $this->pdo->query($sql, $params);
    }

    public function findLike($str, $field, $table = '')
    {
        $table = $table ?: $this->table;
        $sql = "SELECT * FROM $table WHERE $field LIKE ?";
        return $this->pdo->query($sql, ['%' . $str . '%']);
    }

    public function insert($fields)
    {
        $set = $this->set($fields);

        $sql = "INSERT INTO {$this->table} SET {$set['fields']}";

        return $this->pdo->execute($sql, $set['value']);
    }

    public function update($fields, $where = '', $values = [])
    {
        $set = $this->set($fields);

        if (!empty($where)){
            foreach ($values as $value)
            $set['value'][] = $value;
        }

        $sql = "UPDATE {$this->table} SET {$set['fields']} $where";

        return $this->pdo->execute($sql, $set['value']);
    }

    protected function set($fields){
        $set['fields'] = '';
        $set['value'] = [];

        foreach($fields as $field => $value){
            $set['fields'] .= $field . ' = ?, ';
            $set['value'][] = $value;
        }

        $set['fields'] = rtrim($set['fields'], ', ');

        return $set;
    }

}