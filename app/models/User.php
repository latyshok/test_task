<?php
/**
 * Created by PhpStorm.
 * User: black
 * Date: 02.03.2019
 * Time: 15:20
 */

namespace app\models;


use vendor\core\base\Model;

class User extends Model
{
    protected $table = 'users';
    protected $salt;

    const UNIQ_ID = '04e174b22e9269646c9507de5cfd6651';


    public function registration($data)
    {
        return $this->insert([
            'email' => htmlspecialchars($data['email']),
            'hash' => $this->generateHash($data['password']),
            'cookie' => md5($data['email'])
        ]);
    }

    /**
     * генератор случайных символов
     * @param $count
     * @param array $characters
     * @return mixed|string
     */
    public function randString($count, array $characters = [])
    {
        $randstring = '';

        if( !count( $characters ) )
            $characters = ["abcdefghijklnmopqrstuvwxyz", "ABCDEFGHIJKLNMOPQRSTUVWXYZ", "0123456789", ",.<>/?;:[]{}\\|~!@#\$%^&*()-_+="];

        for ($i = 0; $i < $count; $i++) {
            $element = $characters[rand(0, count($characters) - 1)];
            $randstring .= $element[rand(0, strlen($element) - 1)];
        }

        return $randstring;
    }


    /**
     * хэширование пароля
     * @param $password
     * @return string
     */
    public function generateHash($password)
    {
        $this->salt = $this->randString(8);
        return  $this->salt.md5($this->salt.$password);
    }

    /**
     * Проверка введенного пароля со значением в БД
     * @param $id
     * @param $password
     * @return bool
     */
    public function checkHash($hash, $password)
    {
        $salt = substr($hash, 0, (strlen($hash) - 32));
        $realPassword = substr($hash, -32);
        $password = md5($salt.$password);

        if($password == $realPassword) {
            return true;
        }

        return false;
    }

    /**
     * Получить пароль из бд
     * @param $id
     * @return bool
     */
    protected function getPasswordHash( $id )
    {
        $db = MysqliDb::getInstance();

        $res = $db->where('id', $id)->getOne('users', 'pass_hash');

        return $res['pass_hash'] ?: false;
    }

    /**
     * @return mixed|string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    public function login($data){
        $result = false;
        $email = !empty(trim($data['email'])) ? trim($data['email']) : null;
        $password = !empty(trim($data['password'])) ? trim($data['password']) : null;

        if (count($user = $this->findOne($email, 'email')))
            $result = $this->checkHash($user[0]['hash'], $password)
                ? $this->auth($user[0])
                : false;

        return $result;
    }

    public function cookieAuth($key)
    {
        return count($user = $this->findOne(substr($key, 8), 'cookie'))
            ? $this->auth($user[0])
            : false;
    }

    protected function auth($user)
    {
        $this->salt = $this->randString(8);
        $auth = $this->salt.md5($user['email']);

        $_SESSION['user']['auth'] = true;
        $_SESSION['user']['id'] = $user['id'];
        $_SESSION['user']['login'] = $user['email'];

        $profile = new Profile();
        $profile = $profile->findOne($_SESSION['user']['id'], 'user_id') ?: false;

        if ($profile)
            $_SESSION['user']['profile'] = true;

        setcookie('auth', $auth,time() + (86400 * 2), '/'); // 86400 = 1 день в секундах

        return true;
    }

}