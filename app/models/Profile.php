<?php

namespace app\models;

use vendor\core\base\Model;

class Profile extends Model
{
    public $table = 'profiles';

    public function create($data)
    {
        return $this->insert([
            'surname' => $this->format($data['surname']),
            'name' => $this->format($data['name']),
            'lastname' => $this->format($data['lastname']),
            'address' => $this->format($data['address']),
            'city' => $this->format($data['city']),
            'gender' => $this->format($data['gender']),
            'birthdate' => $this->format($data['birthdate']),
            'user_id' => $_SESSION['user']['id']
        ]);
    }

    protected function format($str)
    {
        return htmlspecialchars(trim($str));
    }
}