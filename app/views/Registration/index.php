<form id="registration" class="auth" method="post" name="reg">
    <h2>Регистрация</h2>
    <div>
        <input required  type="text" name="email" placeholder="Email">
    </div>
    <div>
        <input required type="password" name="password" placeholder="Пароль">
    </div>
    <div>
        <input required type="password" name="confirm-password" placeholder="Повторите пароль">
    </div>
    <div class="error"></div>
       <div>
           <button>Регистрация</button>
       </div>
    <div class="mt-10">
        <a class="txt2" href="/">У меня уже есть аккаунт</a>
    </div>
</form>

<script>
    document.addEventListener("DOMContentLoaded", function () {
       registration.addEventListener("submit", function (event) {
            event.preventDefault();
            reg(document.forms.reg);
        });
    });
</script>
