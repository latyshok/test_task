<form class="auth" id="authorization" method="post" action="user/login" name="auth">
    <h2>Авторизация</h2>
        <div>
            <input required  type="text" name="email" placeholder="Email">
        </div>
    <div>
        <input required class="input100" type="password" name="password" placeholder="Password">
    </div>
    <div class="error"></div>
   <div>
       <button>Войти</button>
   </div>
    <div class="mt-10">
        <a class="txt2" href="/registration">Создать аккаунт</a>
    </div>
</form>
<script>
    document.addEventListener("DOMContentLoaded", function () {
        authorization.addEventListener("submit", function (event) {
            event.preventDefault();
            auth(document.forms.auth);
        });
    });
</script>