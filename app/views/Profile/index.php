<div class="profile">
    <h1>Личный кабинет</h1>
    <div class="nav">
        <ul>
            <?php if(empty($profile)) {?>
                <li><a href="profile/create">Создать анкету</a></li>
            <?php } else {?>
                <li><a href="profile/view">Просмотр анкеты</a></li>
                <li><a href="profile/edit">Редактирование анкеты</a></li>
                <li><a class="delete" href="profile/remove">Удалить анкету</a></li>
            <?php } ?>
            <li><a href="user/logout">Выйти</a></li>
        </ul>
    </div>
</div>
<?php if(!empty($profile)) {?>
<script>
    document.addEventListener("DOMContentLoaded", function () {
        document.querySelector('.delete').addEventListener("click", function (event) {
            event.preventDefault();
            if (confirm('Удалить анкету ?'))
                del(<?=$profile['id']?>);
        });
    });
</script>
<?php } ?>