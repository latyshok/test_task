<?php $genders = ['Выберите пол', 'Мужской', 'Женский']; ?>
<div class="back"><a href="/profile">← вернуться назад</a></div>
<div class="create">
    <h1>Редактирование анкеты</h1>
    <form id="profile_edit" method="post" action="create" name="edit">
        <div>
            <input required value="<?=$profile['surname']?>" type="text" name="surname" placeholder="Фамилия">
        </div>
        <div>
            <input required value="<?=$profile['name']?>" type="text" name="name" placeholder="Имя">
        </div>
        <div>
            <input required value="<?=$profile['lastname']?>" type="text" name="lastname" placeholder="Отчество">
        </div>
        <div>
            <input value="<?=$profile['city']?>" type="text" name="city" placeholder="Город">
        </div>
        <div>
            <input value="<?=$profile['address']?>" type="text" name="address" placeholder="Адрес">
        </div>
        <div>
            <label for="gender">
                <select name="gender">
                    <?php foreach ($genders as $key => $gender) { ?>
                        <option value="<?=$key?>" <?=$key == $profile['gender'] ? 'selected' : ''?>><?=$gender?></option>
                    <?php } ?>
                </select>
            </label>
        </div>
        <div>
            <input value="<?=$profile['birthdate']?>" required type="date" name="birthdate" placeholder="Дата рождения">
        </div>
        <div class="error"></div>
        <div class="text-center">
            <button class="button">
                <span>Редактировать анкету</span>
            </button>
        </div>
    </form>
</div>
<script>
    document.addEventListener("DOMContentLoaded", function () {
        profile_edit.addEventListener("submit", function (event) {
            event.preventDefault();
            edit(document.forms.edit);
        });
    });
</script>