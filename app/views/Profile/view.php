<?php $genders = ['Выберите пол', 'Мужской', 'Женский']; ?>
<div class="back"><a href="/profile">← вернуться назад</a></div>
<div class="view">
    <h1 class="text-center">Моя анкета</h1>
    <table>
        <tr>
            <td><b>Фамилия: </b></td>
            <td><?=$profile['surname']?></td>
        </tr>
        <tr>
            <td><b>Имя: </b</td>
            <td></b><?=$profile['name']?></td>
        </tr>
        <tr>
            <td><b>Отчество: </b></td>
            <td><?=$profile['lastname']?></td>
        </tr>
        <?php if($profile['gender']) {?>
            <tr>
                <td><b>Пол: </b></td>
                <td><?=$genders[$profile['gender']]?></td>
            </tr>
        <?php } ?>
        <?php if(!empty($profile['city'])) {?>
            <tr>
                <td><b>Город: </b></td>
                <td><?=$profile['city']?></td>
            </tr>
        <?php } ?>
        <?php if(!empty($profile['address'])) {?>
            <tr>
                <td><b>Адрес: </b></td>
                <td><?=$profile['address']?></td>
            </tr>
        <?php } ?>
        <tr>
            <td><b>Дата рождения: </b></td>
            <td><?=$profile['birthdate']?></td>
        </tr>
    </table>
</div>
