<div class="back"><a href="/profile">← вернуться назад</a></div>
<div class="create">
    <h1>Создание анкеты</h1>
    <form id="profile_create" method="post" action="create" name="create">
        <div>
            <input required type="text" name="surname" placeholder="Фамилия">
        </div>
        <div>
            <input required type="text" name="name" placeholder="Имя">
        </div>
        <div>
            <input required type="text" name="lastname" placeholder="Отчество">
        </div>
        <div>
            <input type="text" name="city" placeholder="Город">
        </div>
        <div>
            <input type="text" name="address" placeholder="Адрес">
        </div>
        <div>
            <label for="gender">
                <select name="gender">
                    <option value="0" selected>Выберите пол</option>
                    <option value="1">мужской</option>
                    <option value="2">женский</option>
                </select>
            </label>
        </div>
        <div>
            <input required type="date" name="birthdate" placeholder="Дата рождения">
        </div>
        <div class="error"></div>
        <div class="text-center">
            <button class="button">
                <span>Создать анкету</span>
            </button>
        </div>
    </form>
</div>
<script>
    document.addEventListener("DOMContentLoaded", function () {
        profile_create.addEventListener("submit", function (event) {
            event.preventDefault();
            create(document.forms.create);
        });
    });
</script>