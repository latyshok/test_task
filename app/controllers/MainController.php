<?php

namespace app\controllers;

use app\models\User;

class MainController extends AppController
{
    // public $layout = 'test';

    public function indexAction()
    {
        $this->set(['title' => 'Авторизация']);

        if (empty($_SESSION['user']) || $_SESSION['user']['auth'] == false) {
            if ( !empty($_COOKIE['auth'])){
                $user = new User();
                $user->cookieAuth($_COOKIE['auth']);
            }
        } else
            $this->redirect('/profile');
    }

}
