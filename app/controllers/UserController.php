<?php
/**
 * Created by PhpStorm.
 * User: black
 * Date: 02.03.2019
 * Time: 17:37
 */

namespace app\controllers;


use app\models\User;
use vendor\core\base\Controller;

class UserController extends AppController
{
    public function regAction()
    {
        if ($this->isMethod('post') && $this->isAjax()){
            $this->layout = $status = false;

            if(!$error = $this->validate($_POST)){
                $user = new User;
                $status = $user->registration($_POST);
            }

            header('Content-type:application/json;charset=utf-8');

            die(json_encode([
                'status' => $status,
                'error' => $error
            ]));
        } else
            $this->redirect('/');
    }

    public function loginAction()
    {
        if ($this->isMethod('post')){
            $this->layout = $status = false;
            $user = new User();

            if($user->login($_POST))
                $status = true;

            header('Content-type:application/json;charset=utf-8');

            die(json_encode([
                'status' => $status
            ]));

        } else
            $this->redirect('/');

    }

    public function logoutAction()
    {
        $this->layout = $status = false;

        if(isset($_SESSION['user'])){
            unset($_SESSION['user']);
            setcookie('auth', '',time() -30, '/');
        }

        $this->redirect('/');
    }

    protected function validate($data)
    {
        $err = [];

        $user = new User;

        if(count($user->findLike($data['email'], 'email')))
            $err[] = 'Пользователь ' . $data['email'] . ' уже существует';

        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL))
            $err[] = 'Неверный формат E-mail адреса';

        if($data['confirm-password'] != $data['password'])
            $err[] = 'Пароли не совпадают';

        if(strlen($data['password']) < 8)
            $err[] = 'Длинна пароля должна быть не менее 8 символов';

        return count($err) ? $err : false;
    }


}