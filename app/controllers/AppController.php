<?php

namespace app\controllers;

use vendor\core\base\Controller;

class AppController extends Controller
{
    protected function isMethod($method)
    {
        return strtolower($_SERVER['REQUEST_METHOD']) == strtolower($method);
    }

    protected function isAjax()
    {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == "XMLHttpRequest";
    }

    protected function redirect($path)
    {
        header("Location: $path");
    }

}