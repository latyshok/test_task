<?php

namespace app\controllers;

use app\models\Profile;
use ProfileTrait;

class ProfileController extends AppController
{
    public function __construct($route)
    {
        parent::__construct($route);

        if (empty($_SESSION['user']) || $_SESSION['user']['auth'] == false)
            $this->redirect('/');
    }

    public function indexAction()
    {
        $profile = new Profile();
        $data = $profile->findOne($_SESSION['user']['id'], 'user_id') ?: false;

        $this->set([
            'title' => 'Личный кабинет',
            'profile' => $data[0]
        ]);
    }

    public function viewAction()
    {
        $profile = new Profile();
        $data = $profile->findOne($_SESSION['user']['id'], 'user_id') ?: false;

        $this->set([
            'title' => 'Анкета',
            'profile' => $data[0]
        ]);
    }

    public function createAction()
    {
        if($this->isMethod('post')){
            $this->layout = $status = false;

            if(!$error = $this->validate($_POST)){
                $profile = new Profile();
                $status = $profile->create($_POST);
            }

            header('Content-type:application/json;charset=utf-8');

            die(json_encode([
                'status' => $status,
                'error' => $error
            ]));
        } else {
            if (isset($_SESSION['user']['profile']) && $_SESSION['user']['profile'])
                $this->redirect('/profile');

            $this->set([
                'title' => 'Редактирование анкеты'
            ]);
        }
    }

    public function editAction()
    {
        if($this->isMethod('post')){
            $this->layout = $status = false;

            if(!$error = $this->validate($_POST)){
                $profile = new Profile();
                $status = $profile->update($_POST, "WHERE user_id = ?", [$_SESSION['user']['id']]);
            }

            header('Content-type:application/json;charset=utf-8');

            die(json_encode([
                'status' => $status,
                'error' => $error
            ]));
        } else {
            $profile = new Profile();
            $data = $profile->findOne($_SESSION['user']['id'], 'user_id') ?: false;

            $this->set([
                'title' => 'Редактирование анкеты',
                'profile' => $data[0]
            ]);
        }
    }

    public function delAction(){
        if($this->isMethod('post') && $this->isAjax()){
            $this->layout = $status = false;

            $profile = new Profile();
            unset($_SESSION['user']['profile']);
            $status = $profile->query("DELETE FROM profiles WHERE id = ?", [$_POST['id']]);


            header('Content-type:application/json;charset=utf-8');

            die(json_encode([
                'status' => $status
            ]));
        } else
            $this->redirect('profile');
    }

    protected function validate($data)
    {
        $err = [];

        $profile= new Profile;

        if(!preg_match('/^[А-Яа-я]+$/u', trim($data['surname'])))
            $err[] = 'Фамилия должна состоять из одного слова и содержать только русские буквы';

        if(!preg_match('/^[А-Яа-я]+$/u', trim($data['name'])))
            $err[] = 'Имя должно состоять из одного слова и содержать только русские буквы';

        if(!preg_match('/^[А-Яа-я]+$/u', trim($data['lastname'])))
            $err[] = 'Отчество должно состоять из одного слова и содержать только русские буквы';

        if(!empty(trim($data['city'])) && !preg_match('/^[А-Яа-я]+$/u', trim($data['city'])))
            $err[] = 'Отчество должно состоять из одного слова и содержать только русские буквы';

        if(!preg_match('/^\d{4}-\d{2}-\d{2}$/', trim($data['birthdate'])))
            $err[] = 'Дата должна соответствовать формату гггг-мм-дд';

        return count($err) ? $err : false;
    }

}
